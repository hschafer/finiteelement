\documentclass{article}

\newcommand{\half}[0]{\frac{1}{2}}

\usepackage{Sweave}
\begin{document}
\title{Introduction to Finite Elements:\\
Galerkin's Method using Trigonometic Functions}
\author{H. Sch\"afer}
\maketitle

\section{Overview}

This example is taken from Estep's book section 6.2.4: The strong formulation of the problem is
\begin{eqnarray}
-u^{\prime\prime} &=& f(x)\hfill x\in (0,1)\\
u(0) = u(1) &=& 0\nonumber
\end{eqnarray}
So we have a boundary value problem of the heat equation. We need to fing the weak formulation
of the problem:

\begin{equation}
\int_0^1 u^\prime v^\prime dx = \int_0^1 fv dx
\end{equation}
We use as trial and test function space the series of $\sin\left(i\pi x\right)$ with $n\in 1\dots n$.
They automatically take the boundary conditions into account, they are all zero at $x=0,1$.

For the load function 
\begin{equation}
f(x) = 10\left(1-10\left(x-\half\right)^2\right)\exp\left(-5\left(x-\half\right)^2\right)
\end{equation}
\begin{Schunk}
\begin{Sinput}
> f <- function(x) {
+     10*(1-10*(x-0.5)^2)*exp(-5*(x-0.5)^2)
+ }
\end{Sinput}
\end{Schunk}
the problem has a analytical solution
\begin{equation}
u(x) = e^{-5\left(x-\half\right)^2} = e^{-\frac{5}{4}}
\end{equation}
\begin{Schunk}
\begin{Sinput}
> u <- function(x) exp(-5*(x-0.5)^2)-exp(-5/4)
\end{Sinput}
\end{Schunk}

So the trial function is
\begin{equation}
U(x) = \sum_i^n a_i \sin(i\pi x) = \sum_i^n a_i \phi_i
\end{equation}
The function space is constructed from the basis functions $\phi_i$ with $i=1\dots n$. 
The trial function is part of this function space as its just a linear combination of basis
functions.

Now if we put the trial function into the weak problem we have $n$ unknown coefficients. By
using each $\phi_i$ in turn as a test function we get $n$ equations and thus get a solvable
systemL
\begin{eqnarray}
\int_0^1 \left(\sum_ia_i\phi_i^\prime \right)\phi_j^\prime dx &=& \int_0^1f\phi_jdx\\
\sum_ia_i\int\phi_i^\prime\phi_j^\prime dx &=& \int_0^1f\phi_jdx
\end{eqnarray}

We can formulate this as a matrix equation:
\begin{equation}
\left( 
\begin{array}{ccc}
\int\phi_1^\prime \phi_1^\prime dx & \int\phi_2^\prime \phi_1^\prime dx & \dots \\
\int\phi_1^\prime \phi_2^\prime dx & \int\phi_2^\prime \phi_2^\prime dx & \dots \\
\int\phi_1^\prime \phi_3^\prime dx & \int\phi_2^\prime \phi_3^\prime dx & \dots \\
\multicolumn{3}{c}{\dots}
\end{array}
\right)
\left(
\begin{array}{c}
a_1\\
a_2\\
a_3\\
\dots
\end{array}
\right) =
\left(
\begin{array}{c}
\int f\phi_1 dx\\
\int f\phi_2 dx\\
\int f\phi_3 dx\\
\dots
\end{array}
\right)
\end{equation}

So we have a full matrix here because we use basis functions that are defined over the whole
domain.

The elements of the stiffness matrix are
\begin{equation}
\int \phi_i^\prime\phi_j^\prime dx = \pi^2ij\int \cos\left(i\pi x\right)\cos\left(j\pi x\right) dx
\end{equation}


We calculate the stiffness matrix using numerical integration:
\begin{Schunk}
\begin{Sinput}
> stiffnessmatrix <- function(n) {
+     K <- matrix(NA,n,n)
+     for (i in 1:n) {
+         for (j in 1:n) {
+             K[i,j] <- pi^2*i*j*integrate(function(x) {cos(i*pi*x)*cos(j*pi*x)},0,1)$value
+         }
+     }
+     return(K)
+ }
\end{Sinput}
\end{Schunk}

Same for the load vector:
\begin{Schunk}
\begin{Sinput}
> loadvector <- function(n,f) {
+     b <- matrix(NA,n,1)
+     for (i in 1:n) {
+         b[i] <- integrate(function(x) f(x)*sin(i*pi*x),0,1)$value
+     }
+     return(b)
+ }
\end{Sinput}
\end{Schunk}

Once we have the solution vector $a$ we can get the trial function as:
\begin{Schunk}
\begin{Sinput}
> solution <- function(n,a,x) {
+     y <- 0
+     for (i in 1:n) {
+         y <- y + a[i]*sin(i*pi*x)
+     }
+     return(y)
+ }
\end{Sinput}
\end{Schunk}

Now we solve the example twice, once with 10 functions, then with 20:
\begin{Schunk}
\begin{Sinput}
> n <-10 
> K <- stiffnessmatrix(n)
> b <- loadvector(n,f)
> a10 <- solve(K,b)
> n <-20 
> K <- stiffnessmatrix(n)
> b <- loadvector(n,f)
> a20 <- solve(K,b)
\end{Sinput}
\end{Schunk}
In order to look at the error we look at a very fine grid as an approximation to the 
continuous $x$:
\begin{Schunk}
\begin{Sinput}
> xx <- seq(0,1,length=100)
> u10 <- solution(10,a10,xx)
> u20 <- solution(20,a20,xx)
\end{Sinput}
\end{Schunk}
Now we plot the error:
\begin{Schunk}
\begin{Sinput}
> plot(xx,u(xx)-u10,ty="l")
> lines(xx,u(xx)-u20,col="red")
\end{Sinput}
\end{Schunk}
\includegraphics{fe1-008}

As the basis functions are orthogonal we can increase the used $n$ 
without any danger of getting a singular stiffness matrix.

The error analysis is difficult as this is the error of the
 coefficients {\bf not} the function value approximation.
 I tried to look at the solutions $\pm$ the error in the coefficients but that
 did not look right.


\end{document}
