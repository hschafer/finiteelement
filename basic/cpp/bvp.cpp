#include <iostream>

/*
 * Simple BVP Problem
 *
 * u'' + sin(\pi x) = 0     u(0) = u(1) = 0
 *
 */

#include <gsl/gsl_integration.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <tools/SimplePlot/SimplePlot.hpp>
#include <numlib/functions.hpp>

using namespace std;

class HatFunction {
public:
    // HatFunction i is centered on _grid[i]
    HatFunction(size_t idx, const std::vector<double>& grid) : _idx(idx) {
        _rightHalf = idx == grid.size()-1;
        _lower = idx == 0 ? grid[0] : grid[idx-1];
        _mid = grid[idx];
        _upper = idx == grid.size()-1 ? grid[grid.size()-1] : grid[idx+1];
    }

    double value(double x) {
        if ((x < _lower) || (x>_upper)) return 0.0;
        if (_rightHalf && (x <= _mid)) { // this is rather shitty!!!!
            return (x - _lower) / (_mid - _lower);
        }
        if (x < _mid) {
            return (x - _lower) / (_mid - _lower);
        }
        return (_upper - x)/(_upper-_mid);
    }

    double deriv(double x) {
        if ((x < _lower) || (x>_upper)) return 0.0;
        if (_rightHalf && (x <= _mid)) { // this is rather shitty!!!!
            return 1.0 / (_mid - _lower);
        }
        if (x < _mid) {
            return 1.0 / (_mid - _lower);
        }
        return - 1.0/(_upper-_mid);
    }

private:
    size_t _idx; // index of this Hat Function
    double _lower;
    double _mid;
    double _upper;
    bool _rightHalf;
};

double loadFunction(double x, void *p) {
    HatFunction* phi = static_cast<HatFunction*>(p);
    return std::sin(numlib::PI*x)*phi->value(x);
}

double matrixElement(double x, void *p) {
    std::pair<HatFunction,HatFunction>* phis = static_cast<std::pair<HatFunction,HatFunction>*>(p);
    return phis->first.deriv(x)*phis->second.deriv(x);
}

int main() {

    std::vector<double> grid {0.0, 0.1, 0.25, 0.5, 0.54, 0.75, 0.9, 0.95, 1.0};
    //std::vector<double> grid {0.0, 0.1, 0.5, 0.75, 1.0};
    //std::vector<double> grid {0.0, 0.25, 0.5, 0.75, 1.0};

    std::vector<HatFunction> basis;
    // boundary condition means we skip first and last
    for(size_t i=1; i<grid.size()-1; i++) {
        basis.push_back(HatFunction(i, grid));
    }

    size_t dof = basis.size();
    // construct the load vector
    size_t quadSteps = 100;
    gsl_integration_workspace* work = gsl_integration_workspace_alloc(quadSteps);
    gsl_vector* load = gsl_vector_alloc(dof);
    for(size_t i=0; i<dof; i++) {
        double sum, err;
        gsl_function f;
        f.function = &loadFunction;
        f.params = &(basis[i]);
        auto status = gsl_integration_qag(&f, grid[0], grid[grid.size()-1], 0.001, 0.001, quadSteps, 1, work, &sum, &err);
        std::cerr << i << "\t" << sum << std::endl;
        gsl_vector_set(load, i, sum);
    }

    // build the stiffness matrix
    gsl_matrix* stiff = gsl_matrix_alloc(dof, dof);
    for(size_t i=0; i<dof; i++) {
        for(size_t j=0; j<dof; j++) {
            auto phis = std::make_pair(basis[i], basis[j]);
            double sum, err;
            gsl_function f;
            f.function = &matrixElement;
            f.params = &(phis);
            auto status = gsl_integration_qag(&f, grid[0], grid[grid.size()-1], 0.001, 0.001, quadSteps, 1, work, &sum, &err);
            std::cerr << sum << " ";
            gsl_matrix_set(stiff, i, j, sum);
        }
        std::cerr << std::endl;
    }

    // solve the system
    gsl_permutation* perm = gsl_permutation_alloc(dof);

    int sig;
    gsl_linalg_LU_decomp(stiff, perm , &sig);

    gsl_vector* res = gsl_vector_alloc(dof);
    gsl_linalg_LU_solve(stiff, perm, load, res);

    SimplePlot pltx(".", "res.png", 2, SimplePlot::PNG);
    double x = 0.0;
    while (x <= 1.0) {
        double ref = sin(numlib::PI*x)/(numlib::PI*numlib::PI);
        double approx = 0.0;
        for(size_t i=0; i<dof; i++) {
            approx += gsl_vector_get(res, i)*basis[i].value(x);
        }
        pltx.Add(x, ref, approx);
        x += 0.01;
    }
    pltx.Plot();

    // now we should calculate the residual and estimate the error

    gsl_permutation_free(perm);
    gsl_matrix_free(stiff);
    gsl_integration_workspace_free(work);
    gsl_vector_free(res);
    gsl_vector_free(load);
    return EXIT_SUCCESS;
}
